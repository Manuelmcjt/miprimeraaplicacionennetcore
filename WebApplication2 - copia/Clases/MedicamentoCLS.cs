﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Clases
{
    public class MedicamentoCLS
    {
        [Display (Name ="id Medicamento")]
        public int iidmedicamento { get; set; }
        [Display(Name = "Nombre Medicamento")]
        public string nombre { get; set; }
        [Display(Name = "Precio")]
        public decimal precio { get; set; }
        [Display(Name = "Stock")]
        public int stock { get; set; }
        [Display(Name = "Nombre Forma Farmaceutica")]
        public string nombreFormaFarmaceutica { get; set; }
        public int iidFormaFarmaceutica { get; set; }

    }
}