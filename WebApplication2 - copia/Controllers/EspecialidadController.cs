﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Clases;

namespace WebApplication2.Controllers
{
    public class EspecialidadController : Controller
    {
        // GET: Especialidad
        public ActionResult Index(EspecialidadCLS  oEspecialidadCLS)
        {
            List<EspecialidadCLS> listaEspecialidad = new List<EspecialidadCLS>();
            //ViewBag.Mensaje = "Estoy pasando un mensaje del controller a la vista";
            using (BDHospitalEntities1 db = new BDHospitalEntities1())
            {
                if (oEspecialidadCLS.nombre == null || oEspecialidadCLS.nombre == "")
                {

                    listaEspecialidad = (from especialidad in db.Especialidad
                                         where especialidad.BHABILITADO == 1
                                         select new EspecialidadCLS
                                         {
                                             iidespecialidad = especialidad.IIDESPECIALIDAD,
                                             nombre = especialidad.NOMBRE,
                                             descripcion = especialidad.DESCRIPCION
                                         }).ToList();
                    ViewBag.nombreEspecialidad = "";
                }
                else
                {

                    listaEspecialidad = (from especialidad in db.Especialidad
                                         where especialidad.BHABILITADO == 1
                                         && especialidad.NOMBRE.Contains(oEspecialidadCLS.nombre)
                                         select new EspecialidadCLS
                                         {
                                             iidespecialidad = especialidad.IIDESPECIALIDAD,
                                             nombre = especialidad.NOMBRE,
                                             descripcion = especialidad.DESCRIPCION
                                         }).ToList();
                    ViewBag.nombreEspecialidad = oEspecialidadCLS.nombre;

                }
            
            }
                return View(listaEspecialidad);
        }
    }
}